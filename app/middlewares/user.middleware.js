const db = require("../models");
const jwt = require("jsonwebtoken");
const userModel = require("../models/user.model");

const User = db.user;
const ROLES = db.ROLES;

const verifyToken = async (req, res, next) => {
    console.log('verify token ...');
    let token = req.headers['x-access-token'];
    console.log(token);
    if (!token) {
        return res.status(401).json({
            message:"Token not found!"
        })
    }

    const secretKey = process.env.JWT_SECRET;

    const verified = jwt.verify(token, secretKey);
    console.log(verified);
    if (!verified) {
        return res.status(401).json({
            message:"Token invalid!"
        })
    }

    const user = await userModel.findById(verified.id).populate("roles");
    console.log(user);
    req.user = user;
    
    next();
}


const checkUser = async (req, res, next) => {
    console.log('check user ...');
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i=0; i<userRoles.length; i++) {
            if (userRoles[i].name == 'admin') {
                console.log('authorized!');
                next();
                return;
            }
        }
    }
    console.log('unauthorized!');
    return res.status(401).json({
        message:"Unauthorized!"
    })
}

module.exports = {
    verifyToken,
    checkUser 
}