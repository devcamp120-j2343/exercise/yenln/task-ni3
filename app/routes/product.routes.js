const express = require("express");

const router = express.Router();

const productController = require("../controllers/product.controller");
// const productMiddleWare = require("../middlewares/product.middleware")
const userMiddlewares = require("../middlewares/user.middleware")

router.get('/', userMiddlewares.verifyToken, productController.getAllProduct);
router.post('/', [userMiddlewares.verifyToken, userMiddlewares.checkUser], productController.createProduct);
router.get('/:productid', productController.getProductByName);
router.put('/:productid', productController.updateProductById);
router.delete('/:productid', productController.deleteProductById);

module.exports = router;