const express = require("express");

const router = express.Router();

const authMiddleWare = require("../middlewares/auth.middleware");
const authController = require("../controllers/auth.controller");

// API signup chỉ dành cho user 
router.post("/signup", authMiddleWare.checkDuplicateUsername, authController.signUp);

router.post("/login", authController.logIn);

module.exports = router;