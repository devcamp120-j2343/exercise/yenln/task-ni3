const express = require("express");

const router = express.Router();

const userController = require("../controllers/user.controller");
const userMiddleWare = require("../middlewares/user.middleware")

router.get('/', userMiddleWare.verifyToken, userController.getAllUser);
router.post('/', [userMiddleWare.verifyToken, userMiddleWare.checkUser], userController.createUser);
router.get('/:userid', userController.getUserById);
router.put('/:userid', userController.updateUser);
router.delete('/:userid', userController.deleteUser);

module.exports = router;