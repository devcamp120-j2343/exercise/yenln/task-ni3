const ProductModel = require("../models/product.model");
const mongoose = require("mongoose");

const getProductByName = async (req, res) => {
    const _id = req.params.productid;

    if (!_id && !mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "_id invalid !",
        });
    }

    const result = await ProductModel.findOne({ _id});
    if (!result) return res.status(200).json([]);

    return res.status(200).json(result);
};

const createProduct = async (req, res) => {
    // B1 - collect data
    const { name,price  } = req.body;

    // B2 - validate

    try {
        // B3 - save to database
        const result = await ProductModel.create({
            name,
            price
        });
        return res.status(201).json({
            message: "Create Product successful !",
            data: result,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllProduct = async (req, res) => {
    const product = req.query.product;
    console.log("product:::::::::::::::", product);

    let condition = {};
    if (product && !mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            message: "product invalid !",
        });
    }

    if (product) condition.product = product;

    try {
        const result = await ProductModel.find(condition);
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateProductById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.productid;
    const { name,price  } = req.body;

    if (!_id && !mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    // B2 - validate
    if (!name && !mongoose.Types.ObjectId.isValid(name)) {
        return res.status(400).json({
            message: "name id invalid !",
        });
    }

    try {
        const result = await ProductModel.findByIdAndUpdate(
            _id,
            { name,price  } ,
            {
                new: true,
            }
        );
        if (result) {
            return res.status(200).json({
                message: "Update product successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't product!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deleteProductById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.productid;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await ProductModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta dice history successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find dice history",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getRandomDice = async (url) => {
    return (await fetch(url)).json();
};

module.exports = {
    createProduct,
    getAllProduct,
    deleteProductById,
    updateProductById,
    getProductByName,
};
