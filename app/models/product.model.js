const mongoose = require("mongoose");

const productModel = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    }, 
    price: {
        type: Number,
        required: true
    },
    create_at: {
        type: Date,
        default: Date.now()
    },
    update_at: {
        type: Date,
        default: Date.now()
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Product", productModel);
